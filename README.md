# README #

The solution implementation is a RESTful web service that queries the NASA patents and determines the number of palindromic strings that can be created from innovator's full name. This project is built using Eclipse, Spring Boot, Thymeleaf template engine, Bootstrap and Maven.