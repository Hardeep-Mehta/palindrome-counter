package com.hmehta.restcontroller;

import java.util.List;
import org.junit.Test;
import org.junit.Before;
import java.util.ArrayList;
import java.util.Collections;
import com.hmehta.util.Constants;
import org.springframework.util.Assert;
import com.hmehta.AbstractControllerTest;
import org.springframework.http.MediaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hmehta.service.impl.PalindromeServiceImpl;
import org.springframework.test.web.servlet.MvcResult;
import com.hmehta.model.palindrome.InnovatorPalindromeCount;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

public class PalindromeRestControllerTests extends AbstractControllerTest {

	@Before
	public void setup() {
		super.setup();
	}

	@Test
	public void testGetPalindromes() throws Exception {
		String[] innovatorNames = { "Graham Bell", "Ted Miles" };

		List<InnovatorPalindromeCount> ipc = new ArrayList<InnovatorPalindromeCount>();
		for (String innovatorName : innovatorNames) {
			innovatorName = innovatorName.replaceAll(Constants.SPACE, Constants.EMPTY).trim().toUpperCase();
			Integer palindromeCount = new PalindromeServiceImpl().getPalindromePermutationsCount(innovatorName);
			ipc.add(new InnovatorPalindromeCount(innovatorName, palindromeCount));
			logger.info("{name=" + innovatorName + ", count=" + palindromeCount + "}");
		}

		Collections.reverse(ipc);
	}

	@Test
	public void testFormValidationPassed() throws Exception {
		String uri = "/submit";
		String expectedRedirectURL = "/palindrome?search=temperature&limit=2&api_key=DEMO_KEY";
		mockMvc.perform(MockMvcRequestBuilders.get(uri).param("search", "temperature").param("limit", "2").accept(MediaType.APPLICATION_JSON))
				.andDo(MockMvcResultHandlers.print()).andExpect(MockMvcResultMatchers.status().is3xxRedirection())
				.andExpect(MockMvcResultMatchers.redirectedUrl(expectedRedirectURL)).andReturn();
	}

	@Test
	public void testFormValidationFailed() throws Exception {
		String uri = "/submit";
		mockMvc.perform(MockMvcRequestBuilders.get(uri).param("search", "").param("limit", "8").accept(MediaType.TEXT_HTML))
				.andDo(MockMvcResultHandlers.print()).andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
				.andExpect(MockMvcResultMatchers.model().hasErrors());
	}

	@Test
	public void testPalindrome() throws Exception {
		String uri = "/palindrome";

		MvcResult mvcResult = mockMvc
				.perform(MockMvcRequestBuilders.get(uri).param("search", "temperature").param("limit", "4").accept(MediaType.APPLICATION_JSON))
				.andDo(MockMvcResultHandlers.print()).andExpect(MockMvcResultMatchers.status().is2xxSuccessful()).andReturn();

		Assert.isTrue(!mvcResult.getResponse().getContentAsString().isEmpty());
		ObjectMapper mapper = new ObjectMapper();
		List<InnovatorPalindromeCount> innovators = mapper.readValue(mvcResult.getResponse().getContentAsString(),
				mapper.getTypeFactory().constructCollectionType(List.class, InnovatorPalindromeCount.class));
		for (InnovatorPalindromeCount ipc : innovators) {
			Assert.notNull(ipc);
			Assert.notNull(ipc.getName());
		}
	}
	
}