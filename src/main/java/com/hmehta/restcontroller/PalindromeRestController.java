package com.hmehta.restcontroller;

import java.util.List;
import java.util.Arrays;
import org.slf4j.Logger;
import java.util.ArrayList;
import java.util.Collections;
import org.slf4j.LoggerFactory;
import com.hmehta.util.Constants;
import com.hmehta.util.Formatter;
import com.hmehta.model.nasa.Results;
import com.hmehta.model.nasa.Innovator;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;
import com.hmehta.model.nasa.NASAPatentQueryResults;
import com.hmehta.service.impl.PalindromeServiceImpl;
import org.springframework.web.client.RestClientException;
import com.hmehta.model.palindrome.InnovatorPalindromeCount;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PalindromeRestController {

	private final String NASA_BASE_URL = "https://api.nasa.gov/patents/content?query=";
	
	private static final Logger logger = LoggerFactory.getLogger(PalindromeRestController.class.getName());

	@RequestMapping(path = "/palindrome", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public List<InnovatorPalindromeCount> getPalindromes(@RequestParam(name = "search", required = true) String search,
			@RequestParam(name = "limit", required = false, defaultValue = "1") int limit) {
		List<Results> resultsList = new ArrayList<>();
		try {
			StringBuilder nasaQueryURL = new StringBuilder(NASA_BASE_URL).append(search).append("&limit=").append(limit).append("&api_key=DEMO_KEY");
			NASAPatentQueryResults queryResults = (new RestTemplate()).getForObject(nasaQueryURL.toString(), NASAPatentQueryResults.class);
			Results[] results = queryResults.getResults();
			if (results != null) {
				resultsList = Arrays.asList(results);
			}
		} catch (RestClientException rce) {
			logger.error(rce.getMessage(), rce);
		}

		List<String> inovatorNames = new ArrayList<String>();
		try {
			for (Results result : resultsList) {
				Innovator[] innovators = result.getInnovator();
				for (Innovator innovator : innovators) {
					String fName = Formatter.removeNull(innovator.getFname());
					String lName = Formatter.removeNull(innovator.getLname());
					StringBuilder fullName = new StringBuilder(fName).append(Constants.SPACE).append(lName);
					inovatorNames.add(fullName.toString());
				}
			}
		} catch (Throwable t) {
			logger.error(t.getMessage(), t);
		}

		List<InnovatorPalindromeCount> ipc = new ArrayList<InnovatorPalindromeCount>();
		for (String innovatorName : inovatorNames) {
			Integer palindromeCount = new PalindromeServiceImpl().getPalindromePermutationsCount(innovatorName);
			ipc.add(new InnovatorPalindromeCount(innovatorName, palindromeCount));
			logger.info("{name=" + innovatorName + ", count=" + palindromeCount + "}");
		}

		Collections.reverse(ipc);
		return ipc;
	}

}