package com.hmehta.service;

public interface PalindromeService {

	int getPalindromePermutationsCount(String s);

}