package com.hmehta.service.impl;

import java.util.Set;
import org.slf4j.Logger;
import java.util.HashSet;
import org.slf4j.LoggerFactory;
import com.hmehta.util.Constants;
import java.util.concurrent.TimeUnit;
import com.hmehta.service.PalindromeService;
import org.springframework.stereotype.Service;
import org.springframework.stereotype.Component;

@Service
@Component
public class PalindromeServiceImpl implements PalindromeService {

	private static Set<String> palindromes = new HashSet<String>();

	private static final Logger logger = LoggerFactory.getLogger(PalindromeServiceImpl.class.getName());

	public int getPalindromePermutationsCount(String s) {
		int count = 0;
		long lStartTime = System.currentTimeMillis();
		s = s.replaceAll(Constants.SPACE, Constants.EMPTY).trim().toLowerCase();
		char[] input = s.toCharArray();
		char[] output = input.clone();
		permute(output, input, 0);
		long lEndTime = System.currentTimeMillis();
		logger.info("There are " + palindromes.size() + " palindromic permutations of '" + s + "', length= " + s.length() + ", timeTaken= "
				+ TimeUnit.MILLISECONDS.toSeconds((lEndTime - lStartTime)) + " seconds.");
		return count;
	}

	private static void permute(char[] output, char[] input, int index) {
		if (index == input.length) {
			if (isPalindrome(output)) {
				palindromes.add(new String(output));
			}
			return;
		}
		for (int i = 0; i < input.length; i++) {
			output[index] = input[i];
			permute(output, input, index + 1);
		}

	}

	private static boolean isPalindrome(char[] s) {
		int n = s.length;
		for (int i = 0; i < (n / 2) + 1; ++i) {
			if (s[i] != s[n - i - 1]) {
				return false;
			}
		}
		return true;
	}

}