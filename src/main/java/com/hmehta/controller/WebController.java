package com.hmehta.controller;

import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;

import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.hmehta.model.web.FormInput;

@Controller
public class WebController {

	@RequestMapping("/")
	public String index(Model model) {
		model.addAttribute("formInput", new FormInput());
		return "index";
	}

	@RequestMapping("/submit")
	public String palindrome(@Valid FormInput formInput, BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			return "index";
		}

		return "redirect:/palindrome?search=" + formInput.getSearch() + "&limit=" + formInput.getLimit() + "&api_key=DEMO_KEY";
	}
}