package com.hmehta.model.web;

import javax.ws.rs.DefaultValue;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import org.springframework.stereotype.Component;
import org.hibernate.validator.constraints.NotEmpty;

@Component
public class FormInput {

	@NotNull(message = "Please provide a search term.")
	@NotEmpty(message = "Please provide a search term.")
	private String search;

	@Min(1)
	@Max(5)
	@DefaultValue("1")
	private Integer limit;

	public String getSearch() {
		return search;
	}

	public void setSearch(String search) {
		this.search = search;
	}

	public Integer getLimit() {
		return limit;
	}

	public void setLimit(Integer limit) {
		this.limit = limit;
	}
}