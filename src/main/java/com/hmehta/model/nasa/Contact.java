package com.hmehta.model.nasa;

public class Contact {
	private String office;

	private String phone;

	private String facility;

	private String address;

	private String email;

	public String getOffice() {
		return office;
	}

	public void setOffice(String office) {
		this.office = office;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getFacility() {
		return facility;
	}

	public void setFacility(String facility) {
		this.facility = facility;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		return "ClassPojo [office = " + office + ", phone = " + phone + ", facility = " + facility + ", address = " + address + ", email = " + email + "]";
	}
}