package com.hmehta.model.nasa;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Results {

	private String patent_number;

	private String[] eRelations;

	private String trl;

	private Contact contact;

	private String serial_number;

	private String id;

	private String center;

	private String reference_number;

	private String category;

	private String title;

	private String _id;

	private String client_record_id;

	private String expiration_date;

	private Innovator[] innovator;

	public String getPatent_number() {
		return patent_number;
	}

	public void setPatent_number(String patent_number) {
		this.patent_number = patent_number;
	}

	public String[] getERelations() {
		return eRelations;
	}

	public void setERelations(String[] eRelations) {
		this.eRelations = eRelations;
	}

	public String getTrl() {
		return trl;
	}

	public void setTrl(String trl) {
		this.trl = trl;
	}

	public Contact getContact() {
		return contact;
	}

	public void setContact(Contact contact) {
		this.contact = contact;
	}

	public String getSerial_number() {
		return serial_number;
	}

	public void setSerial_number(String serial_number) {
		this.serial_number = serial_number;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCenter() {
		return center;
	}

	public void setCenter(String center) {
		this.center = center;
	}

	public String getReference_number() {
		return reference_number;
	}

	public void setReference_number(String reference_number) {
		this.reference_number = reference_number;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String get_id() {
		return _id;
	}

	public void set_id(String _id) {
		this._id = _id;
	}

	public String getClient_record_id() {
		return client_record_id;
	}

	public void setClient_record_id(String client_record_id) {
		this.client_record_id = client_record_id;
	}

	public String getExpiration_date() {
		return expiration_date;
	}

	public void setExpiration_date(String expiration_date) {
		this.expiration_date = expiration_date;
	}

	public Innovator[] getInnovator() {
		return innovator;
	}

	public void setInnovator(Innovator[] innovator) {
		this.innovator = innovator;
	}

	@Override
	public String toString() {
		return "ClassPojo [abstract = -, patent_number = " + patent_number + ", eRelations = " + eRelations + ", trl = " + trl + ", contact = " + contact + ", concepts = -, serial_number = "
				+ serial_number + ", publication = -, id = " + id + ", center = " + center + ", reference_number = " + reference_number + ", category = " + category + ", title = " + title
				+ ", _id = " + _id + ", client_record_id = " + client_record_id + ", expiration_date = " + expiration_date + ", innovator = " + innovator + "]";
	}
}