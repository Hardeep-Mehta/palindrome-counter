package com.hmehta.model.nasa;

public class NASAPatentQueryResults {
	private String count;
	private Results[] results;

	public Results[] getResults() {
		return results;
	}

	public void setResults(Results[] results) {
		this.results = results;
	}

	public String getCount() {
		return count;
	}

	public void setCount(String count) {
		this.count = count;
	}

	@Override
	public String toString() {
		return "ClassPojo [results = " + results + ", count = " + count + "]";
	}
}
