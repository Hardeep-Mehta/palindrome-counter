package com.hmehta.model.nasa;

public class Innovator {
	private String mname;

	private String lname;

	private String order;

	private String company;

	private String fname;

	public String getMname() {
		return mname;
	}

	public void setMname(String mname) {
		this.mname = mname;
	}

	public String getLname() {
		return lname;
	}

	public void setLname(String lname) {
		this.lname = lname;
	}

	public String getOrder() {
		return order;
	}

	public void setOrder(String order) {
		this.order = order;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getFname() {
		return fname;
	}

	public void setFname(String fname) {
		this.fname = fname;
	}

	@Override
	public String toString() {
		return "ClassPojo [mname = " + mname + ", lname = " + lname + ", order = " + order + ", company = " + company + ", fname = " + fname + "]";
	}
}