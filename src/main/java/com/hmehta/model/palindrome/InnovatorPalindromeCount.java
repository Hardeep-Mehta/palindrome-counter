package com.hmehta.model.palindrome;

import org.springframework.stereotype.Component;

@Component
public class InnovatorPalindromeCount implements Comparable<InnovatorPalindromeCount> {

	private String name = null;
	private int count = 0;

	public InnovatorPalindromeCount() {
	}

	public InnovatorPalindromeCount(String name, int count) {
		this.name = name;
		this.count = count;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public int compareTo(InnovatorPalindromeCount ipc) {
		if ((ipc != null) && (this.getCount() >= ipc.getCount())) {
			return 1;
		} else {
			return 0;
		}
	}

}